import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from 'angularfire2/firestore';


@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private ordersCollection: AngularFirestoreCollection<any>;

  constructor(private afs: AngularFirestore) { 
    this.ordersCollection = afs.collection<any>('orders');
  }

  public orders() {
    return this.ordersCollection;
  }

   //index
   public getOrders() {
    return this.afs.collection('orders').snapshotChanges();
  }

  //create
  public createOrder(data: {
    economicNumber: string,
    quantity: number,
    order: string,
    status: string
  }) {
    return this.afs.collection('orders').add(data);
  }
}

import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  displayedColumns: string[] = ['economicNumber', 'quantity', 'order', 'status' ];
  dataSource =  [];
  
  constructor(public ordersService: OrdersService,) { }

  ngOnInit(): void {
    this.ordersService.orders().valueChanges().subscribe( (ordersSnapshot) => {
      this.dataSource = [];
      ordersSnapshot.forEach((ordersData: any) => {
        this.dataSource.push(ordersData);
        
      });
      
    });

  }

}

import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root',
})
export class SalesService {
  private salesCollection: AngularFirestoreCollection<any>;

  constructor(private afs: AngularFirestore) {
    this.salesCollection = afs.collection<any>('sales');
  }

  //index
  public getSales() {
    return this.afs.collection('sales').snapshotChanges();
  }


  public getSalesCollection() {
    return this.salesCollection;
  }

  //create
  public async createSale(data: {
    economicNumber: string,
    unitType: string,
    initial: number,
    final: number,
    quantity: number,
    product: string
    kgs: number,
    subtotal: number,
    total: number,
    rotogauge: number
  }) {
    const id = this.afs.createId();
    const sale = {...data, id};
    return new Promise((resolve, reject) => {
      this.afs.collection('sales').doc(id).set(data).then(() => {
        resolve(sale);
      });
    });
  }

  //show
  public getSale(documentId: string) {
    return this.afs.collection('sales').doc(documentId).snapshotChanges();
  }

  //update
  public updateSale(
    documentId: string,
    data: {
      economicNumber?: string,
      unitType?: string,
      initialLiters?: number,
      finalLiters?: number,
      quantity?: number,
      product?: string,
      kgs?: number,
      subtotal?: number,
      total?: number,
      rotogauge?: number,
    }
  ) {
    return this.afs.collection('sales').doc(documentId).set(data);
  }

  //delete
  public deleteSale(documentId: string) {
    return this.afs.collection('sales').doc(documentId).delete();
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { SalesService } from '../../services/sales.service';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Types } from 'src/app/models/types';
import { Products } from 'src/app/models/products';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss'],
})
export class SalesComponent implements OnInit {
  public sales = [];
  saleForm: FormGroup;
  types: Types[] = [];
  products: Products[] = [];
  isGasPipe: boolean = false;
  

  constructor(
    public salesService: SalesService,
    public ordersService: OrdersService,
    public dialogRef: MatDialogRef<SalesComponent>,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.saleForm = this.formBuilder.group({
      economicNumber: [null, Validators.required],
      unitType: [null, Validators.required],
      initialLiters: [null, Validators.required],
      finalLiters: [null, Validators.required],
      quantity: [null, Validators.required],
      product: [null, Validators.required],
      kgs: [null, Validators.required],
      subtotal : [null, Validators.required],
      total :  [null, Validators.required],
      rotogauge : [null]
    });

    this.types = [
      { id: 1, name: 'estacionario', description: 'Estacionario' },
      { id: 2, name: 'portatil', description: 'Portátil' },
    ];

    this.salesService.getSales().subscribe();
  }

  selectType(type) {
    if (type == 1) {
      this.products = [{ id: 1, name: 'LTS', type: 1, price: 20 }];
      this.saleForm.controls['initialLiters'].setValue(null);
      this.saleForm.controls['finalLiters'].setValue(null);
      this.isGasPipe = true;
    } else {
      this.products = [
        { id: 2, name: 'CIL30', type: 2, price: 520 },
        { id: 3, name: 'CIL45', type: 2, price: 650 },
        { id: 4, name: 'CIL10', type: 2, price: 200 },
      ];

      this.isGasPipe = false;
      this.saleForm.controls['initialLiters'].setValue(null);
      this.saleForm.controls['finalLiters'].setValue(null);
    }
  }

  selectProduct(id){
    this.products.forEach((product : Products) => {
      if(product.id == id){
        this.saleForm.controls['subtotal'].setValue(product.price * this.saleForm.controls['quantity'].value);
        this.saleForm.controls['total'].setValue(this.saleForm.controls['subtotal'].value * 1.16); 

        if(product.type == 1){
          var totalLiters = this.saleForm.controls['finalLiters'].value - this.saleForm.controls['initialLiters'].value;
          this.saleForm.controls['kgs'].setValue(totalLiters * 0.54);
          
          var capacity = Math.floor(Math.random() * 500) + 200;
          this.saleForm.controls['rotogauge'].setValue((this.saleForm.controls['finalLiters'].value) / capacity);
        }else{
          this.saleForm.controls['kgs'].setValue(this.saleForm.controls['initialLiters'].value - this.saleForm.controls['finalLiters'].value);
        }
      }
    });
  }

  onSubmit() {
    var formData = {
      economicNumber: this.saleForm.controls['economicNumber'].value,
      unitType: this.saleForm.controls['unitType'].value,
      initial: this.saleForm.controls['initialLiters'].value,
      final: this.saleForm.controls['finalLiters'].value,
      quantity: this.saleForm.controls['quantity'].value,
      product: this.saleForm.controls['product'].value,
      kgs: this.saleForm.controls['kgs'].value,
      subtotal : this.saleForm.controls['subtotal'].value,
      total :  this.saleForm.controls['total'].value,
      rotogauge: this.saleForm.controls['rotogauge'].value
    };


    this.salesService.createSale(formData).then((data) => {
      var order = {
        economicNumber: data['economicNumber'],
        quantity: data['quantity'],
        order: data['id'],
        status: "Cargando"
      }
      this.ordersService.createOrder(order).then(() => {
        this.dialogRef.close();
      });
    });
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
